import React, { Component } from "react";
import './Nav.css';
import logo from '../images/image_2023_06_30T06_00_18_590Z.png';
import { useState } from 'react';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Modal from 'react-bootstrap/Modal';
// import smartphone from "../Images/smartphone.avif";




function Nav(){
   

      const [show, setShow] = useState(false);

      const handleClose = () => setShow(false);
      const handleShow = () => setShow(true);
    

       function handlelogo(){
        window.location.href='./';
       }
        return(
              <div>
                  <div>
                      <nav class="navbar navbar-expand-lg navbar-light bg-color fixed-top nav_Header">
                        <div class="container">
                        <div class="navbar navbar-inverse" role="navigation">       
                      </div> 
                          <img src={logo} width={70} height={70} onClick={handlelogo}/>
                          <h6 style={{color:'goldenrod'}}>HCC TRAVELLING SERVICES</h6>
                          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                          </button>
                          <div class="collapse navbar-collapse" id="navbarResponsive">
                            <ul class="navbar-nav ms-auto Header_inputs">
                              <li class="nav-item active">
                                <b><a href="./Offer" class="nav-link" style={{color:'goldenrod'}}>Offer</a></b>
                              </li>
                              <li class="nav-item ">
                                <b><a  href='./GetFreeRides' class="nav-link" style={{color:'goldenrod'}}>Get Free Rides</a></b>
                              </li>
                              <li class="nav-item ">
  
                              </li>
                              <li class="nav-item ">
                                <b><a href="" class="nav-link" style={{color:'goldenrod'}}>Bookings</a></b>
                              </li>
                              <li class="nav-item ">
                              <>
                                <Button variant="primary" onClick={handleShow}>
                                  Login/Register
                                </Button>
                                <Modal show={show} onHide={handleClose}>
                                  <Modal.Header closeButton>
                                    <Modal.Title className="header">Login/Signup </Modal.Title>
                                  </Modal.Header>
                                  <Modal.Body>
                                   
                                   
                                    <Form>
                                      <Form.Group className="" controlId="exampleForm.ControlInput1">
                                        <Form.Label></Form.Label>
                                        <Form.Control
                                          type="text"
                                          placeholder="Enter your mail "
                                          autoFocus
                                        /><br/>
                                         <Form.Control
                                          type="password"
                                          placeholder="Enter your password "
                                          autoFocus
                                        /><br/>
                                        

                                        <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault"/>
                                        <label class="form-check-label" for="flexCheckDefault">
                                            Get First Ride FREE
                                        </label>
                                        <br/><br/>
                                      <Button variant="danger">
                                        Login/Signup 
                                      </Button>
                                      <br/><br/>
                                      </div>
                                      </Form.Group>
                                    </Form>
                                    
                                  </Modal.Body>
                                </Modal>
                              </>
                              </li>  
                            </ul>
                          </div>
                        </div>
                      </nav>
                  </div>                       
              </div>
            )
    }

export default Nav ;